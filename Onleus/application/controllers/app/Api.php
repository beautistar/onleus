<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//include("./vendor/autoload.php");

class Api extends CI_Controller {

    

    function __construct(){

        parent::__construct();
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('app/api_model', 'api_model');
        $this->load->library('session');     
    }
    
    /**
     * Make json response to the client with result code message
     *
     * @param p_result_code : Result code
     * @param p_result_msg : Result message
     * @param p_result : Result json object
     */

    private function doRespond($p_result_code,  $p_result){

         $p_result['result_code'] = $p_result_code;

         $this->output->set_content_type('application/json')->set_output(json_encode($p_result));
    }

    /**
     * Make json response to the client with success.
     * (result_code = 0, result_msg = "success")
     *
     * @param p_result : Result json object
     */

    private function doRespondSuccess($result){

        $result['message'] = "Success";
        $this->doRespond(200, $result);
    }

    /**
     * Url decode.
     *
     * @param p_text : Data to decode
     *
     * @return text : Decoded text
     */

    private function doUrlDecode($p_text){

        $p_text = urldecode($p_text);
        $p_text = str_replace('&#40;', '(', $p_text);
        $p_text = str_replace('&#41;', ')', $p_text);
        $p_text = str_replace('%40', '@', $p_text);
        $p_text = str_replace('%20',' ', $p_text);
        $p_text = trim($p_text);


        return $p_text;
    }
    
    function version() {
        phpinfo();
    }
    
    /**
    * get user object from query array result
    * 
    * @param mixed $user_array query result array
    */
    function getUser($user_array) {
        
        $user_object = array('user_id' => $user_array['id'],                             
                             'user_name' => $user_array['user_name'],
                             'email' => $user_array['email'],                              
                             'photo_url' => $user_array['photo_url'],
                             'profile_images' => $this->api_model->getProfileImage($user_array['id']),                              
                             );
        return $user_object;
    }
    
    function getAlbumObject($album_array) {
        
        $album_object = array('id' => $album_array['id'],                             
                             'album_name' => $album_array['album_name'],
                             'cover_url' => $album_array['cover_url'],                              
                             );
        return $album_object;
    }
    
    function getPhotoObject($photo_array) {
        
        $album_object = array('id' => $photo_array['id'],                             
                             'photo_url' => $photo_array['photo_url'],                              
                             );
        return $album_object;
    }
    

    function signup() {
        
        $result = array();
        
        $user_name = $_POST['user_name']; 
        $email = $_POST['email'];
        $password = $_POST['password'];
        
        if ($this->api_model->exist_user_name($user_name)) {
             $result['message'] = "user name already exist.";
             $this->doRespond(201, $result);             
        } else {
            $data = array('user_name' => $user_name,                          
                          'email' => $email,
                          'password' => password_hash($password, PASSWORD_BCRYPT),
                          'created_at' => date('Y-m-d h:m:s'),
                          'updated_at' => date('Y-m-d h:m:s')
                          );
            $result["user_id"] = $this->api_model->add_user($data);            
            $this->doRespondSuccess($result);  
        }
    }
    
    function uploadPhoto() {
         
         $result = array();
         
         $user_id = $_POST['user_id'];

                  
         if(!is_dir("uploadfiles/")) {
             mkdir("uploadfiles/");
         }
         $upload_path = "uploadfiles/";  

         $cur_time = time();
         
         $dateY = date("Y", $cur_time);
         $dateM = date("m", $cur_time);
         
         if(!is_dir($upload_path."/".$dateY)){
             mkdir($upload_path."/".$dateY);
         }
         if(!is_dir($upload_path."/".$dateY."/".$dateM)){
             mkdir($upload_path."/".$dateY."/".$dateM);
         }
         
         $upload_path .= $dateY."/".$dateM."/";
         $upload_url = base_url().$upload_path;

        // Upload file. 

        $w_uploadConfig = array(
            'upload_path' => $upload_path,
            'upload_url' => $upload_url,
            'allowed_types' => "*",
            'overwrite' => TRUE,
            'max_size' => "100000KB",
            'max_width' => 3000,
            'max_height' => 4000,
            'file_name' => $user_id.'_'.intval(microtime(true) * 10)
        );

        $this->load->library('upload', $w_uploadConfig);

        if ($this->upload->do_upload('photo')) {

            $file_url = $w_uploadConfig['upload_url'].$this->upload->file_name;
            $data = array('photo_url' => $file_url, 
                          'updated_at' => date('Y-m-d h:m:s')
                          );
            $id = $this->api_model->upload_photo($user_id, $data);
            $result['photo_url'] = $file_url;            
            $this->doRespondSuccess($result);

        } else {

            $this->doRespond(206, $result);// upload fail
            return;
        }     
    }    
    
    function signin() {
        
        $result = array();
        $profile_images = array();
        
        $user_name = $_POST['user_name'];
        $password = $_POST['password'];
            
        $data = array(
            'user_name' => $user_name,
            'password' => $password
        );
        $q_result = $this->api_model->login($data);
        if ($q_result == TRUE) {
            $result['user_model'] = $this->getUser($q_result);
            $this->doRespondSuccess($result);            
        } else{
            $result['message'] = 'Invalid user name or Password!';
            $this->doRespond(202, $result);
        }        
    }
    
    function createAlbum() {
        
        $result = array();
        $user_id = $_POST['user_id'];
        $album_name = $_POST['album_name'];
        
        if(!is_dir("uploadfiles/")) {
             mkdir("uploadfiles/");
        }
        $upload_path = "uploadfiles/";
        $cur_time = time();           
        $dateY = date("Y", $cur_time);
        $dateM = date("m", $cur_time);
        if(!is_dir($upload_path."/".$dateY)){
            mkdir($upload_path."/".$dateY);
        }
        if(!is_dir($upload_path."/".$dateY."/".$dateM)){
            mkdir($upload_path."/".$dateY."/".$dateM);
        }                             
        $upload_path .= $dateY."/".$dateM."/";
        $upload_url = base_url().$upload_path;

        // Upload file. 

        $w_uploadConfig = array(
            'upload_path' => $upload_path,
            'upload_url' => $upload_url,
            'allowed_types' => "*",
            'overwrite' => TRUE,
            'max_size' => "100000KB",
            'max_width' => 3000,
            'max_height' => 4000,
            'file_name' => $user_id.'_'.intval(microtime(true) * 10)
        );

        $this->load->library('upload', $w_uploadConfig);

        if ($this->upload->do_upload('cover_image')) {

            $file_url = $w_uploadConfig['upload_url'].$this->upload->file_name;
            $data = array('cover_url' => $file_url, 
                          'created_at' => date('Y-m-d h:m:s') ,
                          'user_id' => $user_id,
                          'album_name' => $album_name,
                          );
            $this->api_model->create_album($data);
            $result['cover_url'] = $file_url;            
            $this->doRespondSuccess($result);

        } else {

            $this->doRespond(206, $result);// upload fail
            return;
        }
    }
    
    function getAlbumList() {
        
        $result = array();
        $album_list = array();
        
        $qruery_array = $this->api_model->get_album_list();
        
        if (count($qruery_array) == 0) {
            $result['message'] = "No data.";
            $result['album_list'] = $album_list;
            $this->doRespond(204, $result);
        } else {
            
            foreach ($qruery_array as $row) {
                $store = $this->getAlbumObject($row);
                array_push($album_list, $store);
            }
            $result['album_list'] = $album_list;            
            $this->doRespondSuccess($result);            
        } 
    }
    
    function addPhotoToAlbum() {
        
        $result = array();
        
        $album_id = $_POST['album_id'];
                  
        if(!is_dir("uploadfiles/")) {
            mkdir("uploadfiles/");
        }
        $upload_path = "uploadfiles/";  
        $cur_time = time();
        $dateY = date("Y", $cur_time);
        $dateM = date("m", $cur_time);
        if(!is_dir($upload_path."/".$dateY)){
            mkdir($upload_path."/".$dateY);
        }
        if(!is_dir($upload_path."/".$dateY."/".$dateM)){
            mkdir($upload_path."/".$dateY."/".$dateM);
        }
        $upload_path .= $dateY."/".$dateM."/";
        $upload_url = base_url().$upload_path;

        // Upload file. 
        $w_uploadConfig = array(
            'upload_path' => $upload_path,
            'upload_url' => $upload_url,
            'allowed_types' => "*",
            'overwrite' => TRUE,
            'max_size' => "100000KB",
            'max_width' => 3000,
            'max_height' => 4000,
            'file_name' => $album_id.'_'.intval(microtime(true) * 10)
        );

        $this->load->library('upload', $w_uploadConfig);

        if ($this->upload->do_upload('photo')) {

            $file_url = $w_uploadConfig['upload_url'].$this->upload->file_name;
            $data = array('photo_url' => $file_url, 
                          'created_at' => date('Y-m-d h:m:s'),
                          'album_id' => $album_id
                          );
            $id = $this->api_model->addPhotoToAlbum($data);
            $result['photo_url'] = $file_url;            
            $this->doRespondSuccess($result);

        } else {

            $this->doRespond(206, $result);// upload fail
            return;
        }
    }
    
    function getAlbumPhotoList() {
        
        $result = array();
        $photo_list = array();
        
        $album_id = $_POST['album_id'];
        
        $qruery_array = $this->api_model->get_photo_list($album_id);
        
        if (count($qruery_array) == 0) {
            $result['message'] = "No data.";
            $result['photo_list'] = $photo_list;
            $this->doRespond(204, $result);
        } else {
            
            foreach ($qruery_array as $row) {
                $store = $this->getPhotoObject($row);
                array_push($photo_list, $store);
            }
            $result['photo_list'] = $photo_list;            
            $this->doRespondSuccess($result);            
        }
    }
    
    function inviteFacebook() {
        
        $result = array();
        $user_id = $_POST['user_id'];
        $target = $_POST['target'];
        
        $data = array('user_id' => $user_id,
                      'target' => $target,
                      'type' => 'facebook',
                      'created_at' => date('Y-m-d h:m:s')
                      );
        $this->api_model->invite($data);
        $this->doRespondSuccess($result);
    }
    
    function inviteEmail() {
        
        $result = array();
        $user_id = $_POST['user_id'];
        $target = $_POST['target'];
        
        $data = array('user_id' => $user_id,
                      'target' => $target,
                      'type' => 'email',
                      'created_at' => date('Y-m-d h:m:s')
                      );
        $this->api_model->invite($data);
        $this->doRespondSuccess($result);
    }
    
    function invitePhone() {
        
        $result = array();
        $user_id = $_POST['user_id'];
        $target = $_POST['target'];
        
        $data = array('user_id' => $user_id,
                      'target' => $target,
                      'type' => 'phone',
                      'created_at' => date('Y-m-d h:m:s')
                      );
        $this->api_model->invite($data);
        $this->doRespondSuccess($result);
    }
    
    function inviteUsername() {
        
        $result = array();
        $user_id = $_POST['user_id'];
        $target = $_POST['target'];
        
        $data = array('user_id' => $user_id,
                      'target' => $target,
                      'type' => 'username',
                      'created_at' => date('Y-m-d h:m:s')
                      );
        $this->api_model->invite($data);
        $this->doRespondSuccess($result);
    }
    
    function addProfileImage() {
        
        $result = array();
         
         $user_id = $_POST['user_id'];

                  
         if(!is_dir("uploadfiles/")) {
             mkdir("uploadfiles/");
         }
         $upload_path = "uploadfiles/";  

         $cur_time = time();
         
         $dateY = date("Y", $cur_time);
         $dateM = date("m", $cur_time);
         
         if(!is_dir($upload_path."/".$dateY)){
             mkdir($upload_path."/".$dateY);
         }
         if(!is_dir($upload_path."/".$dateY."/".$dateM)){
             mkdir($upload_path."/".$dateY."/".$dateM);
         }
         
         $upload_path .= $dateY."/".$dateM."/";
         $upload_url = base_url().$upload_path;

        // Upload file. 

        $w_uploadConfig = array(
            'upload_path' => $upload_path,
            'upload_url' => $upload_url,
            'allowed_types' => "*",
            'overwrite' => TRUE,
            'max_size' => "100000KB",
            'max_width' => 3000,
            'max_height' => 4000,
            'file_name' => $user_id.'_'.intval(microtime(true) * 10)
        );

        $this->load->library('upload', $w_uploadConfig);

        if ($this->upload->do_upload('image')) {

            $file_url = $w_uploadConfig['upload_url'].$this->upload->file_name;
            $data = array('image_url' => $file_url, 
                          'user_id' => $user_id, 
                          'created_at' => date('Y-m-d h:m:s')
                          );
            $id = $this->api_model->add_profile_image($data);
            $result['image_url'] = $file_url;            
            $this->doRespondSuccess($result);

        } else {

            $this->doRespond(206, $result);// upload fail
            return;
        }
        
    }
    
    function setFollower() {
        
        $result = array();
        $user_id = $_POST['user_id'];
        $target_id = $_POST['target_id'];
        
        $this->api_model->setFollower($user_id, $target_id);
        $this->doRespondSuccess($result);
    }
    
    function setFollowing() {
        
        $result = array();
        $user_id = $_POST['user_id'];
        $target_id = $_POST['target_id'];
        
        $this->api_model->setFollowing($user_id, $target_id);
        $this->doRespondSuccess($result);
    }
    
    function getFollowerList() {
        
        $result = array();
        $followerList = array();
        $user_id = $_POST['user_id'];
        
        $qruery_array = $this->api_model->getFollowerList($user_id);
        
        if (count($qruery_array) == 0) {
            $result['message'] = "No data.";
            $result['follower_list'] = $followerList;
            $this->doRespond(204, $result);
        } else {
            
            foreach ($qruery_array as $row) {
                $store = $this->getUser($row);
                array_push($followerList, $store);
            }
            $result['follower_list'] = $followerList;            
            $this->doRespondSuccess($result);            
        }
    }
    
    function getFollowingList() {
        
        $result = array();
        $followerList = array();
        $user_id = $_POST['user_id'];
        
        $qruery_array = $this->api_model->getFollowingList($user_id);
        
        if (count($qruery_array) == 0) {
            $result['message'] = "No data.";
            $result['following_list'] = $followerList;
            $this->doRespond(204, $result);
        } else {
            
            foreach ($qruery_array as $row) {
                $store = $this->getUser($row);
                array_push($followerList, $store);
            }
            $result['following_list'] = $followerList;            
            $this->doRespondSuccess($result);            
        }
    }
    
    function createGroup() {
        
        $result = array();
        
        $group_name = $_POST['group_name'];
        $group_type = $_POST['group_type'];
                          
        if(!is_dir("uploadfiles/")) {
            mkdir("uploadfiles/");
        }
        $upload_path = "uploadfiles/";  
        $cur_time = time();
        $dateY = date("Y", $cur_time);
        $dateM = date("m", $cur_time);
        if(!is_dir($upload_path."/".$dateY)){
            mkdir($upload_path."/".$dateY);
        }
        if(!is_dir($upload_path."/".$dateY."/".$dateM)){
            mkdir($upload_path."/".$dateY."/".$dateM);
        }
        $upload_path .= $dateY."/".$dateM."/";
        $upload_url = base_url().$upload_path;

        // Upload file. 
        $w_uploadConfig = array(
            'upload_path' => $upload_path,
            'upload_url' => $upload_url,
            'allowed_types' => "*",
            'overwrite' => TRUE,
            'max_size' => "100000KB",
            'max_width' => 3000,
            'max_height' => 4000,
            'file_name' => intval(microtime(true) * 10)
        );

        $this->load->library('upload', $w_uploadConfig);

        if ($this->upload->do_upload('cover_image')) {

            $file_url = $w_uploadConfig['upload_url'].$this->upload->file_name;
            $data = array('photo_url' => $file_url, 
                          'created_at' => date('Y-m-d h:m:s'),
                          'group_name' => $album_id,
                          'group_type' => $group_type
                          );
            $id = $this->api_model->addPhotoToAlbum($data);
            $result['photo_url'] = $file_url;            
            $this->doRespondSuccess($result);

        } else {

            $this->doRespond(206, $result);// upload fail
            return;
        }
        
    }
    
    

    
                                        
}
?>
