<?php
  
  class Api_model extends CI_Model {
      
      function exist_user_name($username) {
          
          $query = $this->db->get_where('tb_user', array('user_name' => $username));
          if ($query->num_rows() == 0){
                return false;
          } else {
                return true;
          }
      }
      
      function add_user($data) {
          
          $this->db->insert('tb_user', $data);
          return $this->db->insert_id();
      }
      
      function upload_photo($user_id, $data) {
        
          $this->db->where('id', $user_id); 
          $this->db->update('tb_user', $data);
          return true;
      }
      
      public function login($data){
            $query = $this->db->get_where('tb_user', array('user_name' => $data['user_name']));
            if ($query->num_rows() == 0){
                return false;
            }
            else{
                //Compare the password attempt with the password we have stored.
                $result = $query->row_array();
                $validPassword = password_verify($data['password'], $result['password']);
                if($validPassword){
                    return $result = $query->row_array();
                }                
            }
      }
      
      function create_album($data) {
          $this->db->insert('tb_album', $data);
          return $this->db->insert_id();
      }
      
      function get_album_list() {
          return $this->db->get('tb_album')->result_array();
      }
      
      function addPhotoToAlbum($data) {
          $this->db->insert('tb_photo', $data);
          return $this->db->insert_id();
      }
      
      function get_photo_list($album_id) {
          return $this->db->where('album_id', $album_id)->get('tb_photo')->result_array();
      }
      
      function invite($data) {
          $this->db->insert('tb_invite', $data);
          return $this->db->insert_id();
      }
      
      function add_profile_image($data) {
          $this->db->insert('tb_profile_image', $data);
          return $this->db->insert_id();
      }
      
      function getProfileImage($user_id) {
          
          $images = array();
          $this->db->select('image_url'); 
          $this->db->from('tb_profile_image');   
          $this->db->where('user_id', $user_id);
          $result = $this->db->get()->result_array();
          foreach ($result as $row) {
              array_push($images, $row['image_url']);
          }
          
          return $images;


      }
      
      function setFollower($user_id, $target_id) {
          $this->db->where('user_id', $user_id);
          $this->db->where('target_id', $target_id);
          
          if ($this->db->get('tb_follower')->num_rows() == 0) {
              $this->db->set('user_id', $user_id);
              $this->db->set('target_id', $target_id);
              $this->db->set('created_at', date('Y-m-d h:m:s'));
              $this->db->insert('tb_follower');
          }
          
      }
      
      function setFollowing($user_id, $target_id) {
          $this->db->where('user_id', $user_id);
          $this->db->where('target_id', $target_id);
          
          if ($this->db->get('tb_following')->num_rows() == 0) {
              $this->db->set('user_id', $user_id);
              $this->db->set('target_id', $target_id);
              $this->db->set('created_at', date('Y-m-d h:m:s'));
              $this->db->insert('tb_following');
          }
      }
      
      function getFollowerList($user_id) {
          
          $query = $this->db->select('tb_user.*')
                        ->from('tb_follower')
                        ->where('tb_follower.user_id', $user_id)
                        ->join('tb_user', 'tb_follower.target_id = tb_user.id')
                        ->get();
          
          return $query->result_array();
      }
      
      function getFollowingList($user_id) {
          
          $query = $this->db->select('tb_user.*')
                        ->from('tb_following')
                        ->where('tb_following.user_id', $user_id)
                        ->join('tb_user', 'tb_following.target_id = tb_user.id')
                        ->get();
          
          return $query->result_array();
      }
      
      
      
      
      
  }
?>
